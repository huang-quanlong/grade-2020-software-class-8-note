<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/9/3
 * Time: 16:33
 */


$dsn = "sqlsrv:Server=LAPTOP-GQ4AGRUA\SQLEXPRESS;Database=Task";
$db = new PDO($dsn, "sa", "123456");

$sql = 'select * from Task order by TaskId desc';
$result = $db->query($sql);
$TaskList = $result->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>任务详情</title>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <script>

        function alertconfirm() {
            var res = confirm("标记为进行中？") ;
            if (res){
              alert("提交成功")  ;
            }


        }


    </script>
</head>
<body>
<div id="container">
    <a href="list.php">返回任务列表</a>

    <form>
        <table class="update">
            <caption>
                <h3>任务详情</h3>
            </caption>
            <tr>
                <td>任务名称：</td>
                <td><?php echo $TaskList['TaskName']?></td>
            </tr>
            <tr>
                <td>任务状态：</td>
                <td><?php if ($TaskList['TaskStatus'] == 1){
                        echo "新创建";
                    }else if ($TaskList['TaskStatus'] == 2) {
                        echo "进行中";
                    }else if ($TaskList['TaskStatus'] == 2) {
                        echo "已完成";
                    }?></td>
            </tr>
            <tr>
                <td>任务内容：</td>
                <td><textarea cols="60" rows="15"><?php echo $TaskList['TaskContent']?></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td>
                      <a class="update_ing" href="biaoji.php?TaskId=<?php echo $TaskList['TaskId']?>"><input type="text" value="标记为进行中" onclick="alertconfirm()"> </a>
                </td>
            </tr>
        </table>
    </form>

</div>
<script src="js/main.js"></script>
</body>
</html>

