<?php


// 连接数据库，查询出所有的班级信息
$dsn = "sqlsrv:Server=LAPTOP-GQ4AGRUA\SQLEXPRESS;Database=Task";
$db = new PDO($dsn, "sa", "123456");

$sql = 'select * from Task order by TaskId desc';
$result = $db->query($sql);
$TaskList = $result->fetchAll(PDO::FETCH_ASSOC);
//var_dump($classList);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <script>

        function alertconfirm() {
            var res = confirm("是否进行删除？") ;
            if (res){
                alert("删除成功")  ;
            }

        }
    </script>
</head>
<body>

<div id="container">
    <div id="login_info">
        欢迎你：admin
        <a href="">退出登录</a>
    </div>
    <div id="choose_div">
        <button id="all" >全选</button>
        <a id="multi_delete" href="detele.php?TaskId=<?php echo $value['TaskId']?>">删除选中任务</a>

        <a id="add" href="add.php" style="float: right">增加任务</a>
    </div>
    <table class="list">
        <tbody>
        <tr>
            <th></th>
            <th>任务id</th>
            <th>任务名称</th>
            <th>任务状态</th>
            <th>创建时间</th>
            <th>修改时间</th>
            <th>操作</th>
        </tr>
        <?php foreach ($TaskList as $key => $value):?>
        <tr>
            <td><input type="checkbox" class="task-checkbox" name="TaskId[]" value=""></td>
            <td><?php echo $value['TaskId']?></td>
            <td><?php echo $value['TaskName']?></td>
            <td><?php if ($value['TaskStatus'] == 1){
                echo "新创建";
                }else if ($value['TaskStatus'] == 2) {
                    echo "进行中";
                }else if ($value['TaskStatus'] == 2) {
                    echo "已完成";
                }?></td>
            <td><?php echo $value['TaskCreateTime']?></td>
            <td><?php echo $value['TaskUpdateTime']?></td>
            <td>
                <a class="update_ing" href="detail.php?TaskId=<?php echo $value['TaskId']?>">详情</a>
                <a class="detele_ing" href="detele.php?TaskId=<?php echo $value['TaskId']?>">
                    <?php if ($value['TaskStatus'] == 1 ||$value['TaskStatus'] == 2){
                        echo "<input type=\"submit\" value=\"删除\" onclick=\"alertconfirm()\">";
                    }?></a>
            </td>
        </tr>
        <?php endforeach;?>
        </tbody>
    </table>
</div>
</body>
</html>

