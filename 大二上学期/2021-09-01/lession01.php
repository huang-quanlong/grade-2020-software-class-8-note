<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/9/1
 * Time: 9:57
 */
//1. 使用正则表达式查询一个字符串是否全部由数字组成，并用php代码实现。
//2. 使用正则表达式查询一个字符串是否全部由字母组成，并用php代码实现。
//3. 使用正则表达式检测一个字符串是邮箱，并用php代码实现。
//4. 使用正则表达式判断一个字符串是手机号，并用php代码实现。
//5. 实务中注册经常需要填写密码，我们需要对密码进行验证，参考如下规则，并用php代码实现。
//	1. 使用正则表达式判断密码只包含数字，并且长度在6~15位。
//	2. 使用正则表达式判断密码只包含数字和字母，并且既有数字也有字母，长度在6~15位。（高能预警）
//	3. 使用正则表达式判断密码只包含数字、字母和特殊字符~!@#$%^&*，并且数字、字母、特殊字符至少包含1个，长度在6~15位。（高能预警）
//6. 上面的代码是用正则表达式进行判断，尝试不用正则表达式进行判断。（高能预警）
//	1. 提示：遍历字符串里面的每个字符，然后逐个进行判断。

$str = "1155";
$result = preg_match("/^\d+$/",$str);
if ($result > 0){
    echo  "字符串全部由数字组成";
}else{
    echo  "字符串不是全部由数字组成";
}
echo "<br>";

$str1 = "abds";
$result = preg_match("/^[a-zA-Z]+$/",$str1);
if ($result > 0){
    echo "字符串由字母组成";
}else{
    echo "字符串不由字母组成";
}
echo "<br>";

$str2 = "313662196@qq.com";
$result = preg_match("/^[0-9a-zA-Z]+@[0-9a-zA-Z-]{1,26}\.com$/",$str2);
if ($result > 0){
    echo "邮箱符合形式";
}else{
    echo "邮箱不符合形式";
}
echo "<br>";

$str3 = "13105081073";
$result = preg_match("/^1\d{10}$/",$str3);
if ($result ){
    echo "手机号符合";
}else{
    echo "手机号不符合";
}
echo "<br>";

$str4 = "wwqw45124$~";
$result1 = preg_match("/^[0-9a-zA-Z~!@#$%^&*]{6,15}$/",$str4);
$result2 = preg_match("/[0-9]/",$str4);
$result3 = preg_match("/[a-zA-Z]/",$str4);
$result4 = preg_match("/[~!@#$%^&*]/",$str4);
if ($result1 && $result2 && $result3 && $result4){
    echo "密码符合格式";
}else{
    echo "密码不符合格式";
}
echo "<br>";





