--------------------
-- 建库部分
--------------------
-- 创建学生管理系统数据库，名称为Student
use master;
go
-- 检测原库是否存在，如果存在则删除
if exists (select * from sys.databases where name = 'Student')
	drop database Student;
go
-- 创建一个库
create database Student;
go
-- 使用这个库
use Student;
go


--------------------
-- 建表部分
--------------------
-- 创建班级表，存储班级信息，其中字段包含：班级id、班级名称
create table Class(
	ClassId int not null identity(1,1),
	ClassName nvarchar(50) not null
);
go

-- 创建学生表，存储学生信息，其中字段保护：学生id、姓名、性别、生日、家庭住址，所属班级id
create table Student (
	StudentId int not null identity(1, 1),
	StudentName nvarchar(50),
	StudentSex tinyint not null,
	StudentBirth date,
	StudentAddress nvarchar(255) not null
);
go

-- 创建课程表，存储课程信息，其中字段包含：课程id、课程名称、课程学分
create table Course(
	CourseId int identity(1,1),
	CourseName nvarchar(50),
	CourseCredit int
);
go

-- 创建班级课程表，存储班级课程信息，其中字段包含：自增id、班级id、课程id
create table ClassCourse(
	ClassCourseId int identity(1,1),
	ClassId int,
	CourseId int
);
go

-- 创建分数表，存储学生每个课程分数信息，其中字段包含：分数id、学生id、课程id、分数
create table Score(
	ScoreId int identity(1,1),
	StudentId int,
	CourseId int,
	Score int
);
go


-- 学生表建好了，细想一下少了一个所属班级的字段
-- 给学生表 Student 增加一个所属班级id字段 
alter table Student add ClassId int not null;
go

----------------------------------------
-- 创建约束部分，使用alter进行修改
----------------------------------------
-- 班级表 ClassId 字段需要设置为主键（主键约束）
alter table Class add constraint PK_Class_ClassId primary key (ClassId);
-- 学生表 StudentId 字段需要设置为主键（主键约束）
alter table Student add constraint PK_Student_StudentId primary key (StudentId);
-- 课程表 CourseId 字段需要设置为主键（主键约束）
alter table Course add constraint PK_Course_CourseId primary key (CourseId);
-- 班级课程表 ClassCourseId 字段需要设置为主键（主键约束）
alter table ClassCourse add constraint PK_ClassCourse_ClassCourseId primary key (ClassCourseId);
-- 分数表 ScoreId 字段需要设置为主键（主键约束）
alter table Score add constraint PK_Score_ScoreId primary key (ScoreId);

-- 学生表 StudentName 不允许为空（非空约束）
alter table Student alter column StudentName nvarchar(50) not null;

-- 班级表 ClassName 需要唯一（唯一约束）
alter table Class add constraint UQ_Class_ClassName unique(ClassName);
-- 课程表 CourseName 需要唯一（唯一约束）
alter table Course add constraint UQ_Course_CourseName unique(CourseName);

-- 学生表 ClassId 增加默认值为0（默认值约束）
alter table Student add constraint DF_Student_ClassId default(0) for ClassId;

-- 学生表 StudentSex 只能为1或者2（Check约束）
alter table Student add constraint CK_Student_StudentSex check(StudentSex=1 or StudentSex=2);
-- 分数表 Score 字段只能大于等于0（check约束）
alter table Score add constraint CK_Score_Score check(Score>=0);
-- 课程表 CourseCredit 字段只能大于0（check约束）
alter table Course add constraint CK_Course_CourseCredit check(CourseCredit>=0);

-- 班级课程表ClassId 对应是 班级表ClassId 的外键 （外键约束）
alter table ClassCourse add constraint FK_ClassCourse_ClassId foreign key (ClassId) references Class(ClassId);
-- 班级课程表CourseId 对应是 课程表CourseId 的外键 （外键约束）
alter table ClassCourse add constraint FK_ClassCourse_CourseId foreign key (CourseId) references Course(CourseId);
-- 分数表StudentId 对应是 学生表StudentId 的外键 （外键约束）
alter table Score add constraint FK_Score_StudentId foreign key (StudentId) references Student(StudentId);
-- 分数表CourseId 对应是 课程表CourseId 的外键 （外键约束）
alter table Score add constraint FK_Score_CourseId foreign key (CourseId) references Course(CourseId);


--------------------
-- 插入数据部分
--------------------
-- 学校开设了3个班级：软件一班、软件二班、计算机应用技术班。请插入班级表相关数据
insert into Class (ClassName) values ('软件一班');
insert into Class (ClassName) values ('软件二班');
insert into Class (ClassName) values ('计算机应用技术班');

-- 软件一班有3个同学，姓名、性别、生日、家庭住址  分别是：
--	刘正、男、2000-01-01、广西省桂林市七星区空明西路10号鸾东小区
--	黄贵、男、2001-03-20、江西省南昌市青山湖区艾溪湖南路南150米广阳小区
--	陈美、女、2000-07-08、福建省龙岩市新罗区曹溪街道万达小区
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('刘正',1,'2000-01-01','广西省桂林市七星区空明西路10号鸾东小区', 1);
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('黄贵',1,'2001-03-20','江西省南昌市青山湖区艾溪湖南路南150米广阳小区', 1);
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('陈美',2,'2000-07-08','福建省龙岩市新罗区曹溪街道万达小区', 1);


-- 软件二班有2个同学，姓名、性别、生日、家庭住址  分别是：
--	江文、男、2000-08-10、安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光
--	钟琪、女、2001-03-21、湖南省长沙市雨花区红花坡社区
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('江文',1,'2000-08-10','安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光', 2);
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('钟琪',2,'2001-03-21','湖南省长沙市雨花区红花坡社区', 2);

-- 计算机应用技术班有4个同学，姓名、性别、生日、家庭住址  分别是：
--	曾小林、男、1999-12-10、安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光
--	欧阳天天、女、2000-04-05、湖北省武汉市洪山区友谊大道与二环线交汇处融侨悦府
--	徐长卿、男、2001-01-30、江苏省苏州市苏州工业园区独墅湖社区
--	李逍遥、男、1999-11-11、广东省广州市白云区金沙洲岛御洲三街恒大绿洲
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('曾小林',1,'1999-12-10','安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光', 3);
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('欧阳天天',2,'2000-04-05','湖北省武汉市洪山区友谊大道与二环线交汇处融侨悦府', 3);
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('徐长卿',1,'2001-01-30','江苏省苏州市苏州工业园区独墅湖社区', 3);
insert into Student (StudentName, StudentSex, StudentBirth, StudentAddress, ClassId) 
    values ('李逍遥',1,NULL,'广东省广州市白云区金沙洲岛御洲三街恒大绿洲', 3);

-- 软件一班开设课程，课程名称和学分分别为：
--	数据库高级应用、3
--	javascript编程基础、3
--	web前端程序设计基础、4
--	动态网页设计.net基础、6
insert into Course(CourseName, CourseCredit) values ('数据库高级应用', 3);
insert into Course(CourseName, CourseCredit) values ('javascript编程基础', 3);
insert into Course(CourseName, CourseCredit) values ('web前端程序设计基础', 4);
insert into Course(CourseName, CourseCredit) values ('动态网页设计.net基础', 6);

insert into ClassCourse (ClassId, CourseId) values (1, 1);
insert into ClassCourse (ClassId, CourseId) values (1, 2);
insert into ClassCourse (ClassId, CourseId) values (1, 3);
insert into ClassCourse (ClassId, CourseId) values (1, 4);

-- 软件二班开设课程，课程名称和学时分别为：
--	数据库高级应用、3
--	javascript编程基础、3
--	web前端程序设计基础、4
--	动态网页设计.net基础、6

insert into ClassCourse (ClassId, CourseId) values (2, 1);
insert into ClassCourse (ClassId, CourseId) values (2, 2);
insert into ClassCourse (ClassId, CourseId) values (2, 3);
insert into ClassCourse (ClassId, CourseId) values (2, 4);

-- 计算机应用技术班开设课程，课程名称和学时分别为：
--	数据库高级应用、3
--	javascript编程基础、3
--	web前端程序设计基础、4
--	动态网页设计php基础、6
insert into Course(CourseName, CourseCredit) values ('动态网页设计php基础', 6);

insert into ClassCourse (ClassId, CourseId) values (3, 1);
insert into ClassCourse (ClassId, CourseId) values (3, 2);
insert into ClassCourse (ClassId, CourseId) values (3, 3);
insert into ClassCourse (ClassId, CourseId) values (3, 5);


-- 考试完成后，各学生各课程得分：
--	刘正、数据库高级应用、80
--	刘正、javascript编程基础、78
--	刘正、web前端程序设计基础、65
--	刘正、动态网页设计.net基础、90
--	黄贵、数据库高级应用、60
--	黄贵、javascript编程基础、77
--	黄贵、web前端程序设计基础、68
--	黄贵、动态网页设计.net基础、88
--	陈美、数据库高级应用、88
--	陈美、javascript编程基础、45
--	陈美、web前端程序设计基础、66
--	陈美、动态网页设计.net基础、75
insert into Score (StudentId, CourseId, Score) values (1, 1, 80);
insert into Score (StudentId, CourseId, Score) values (1, 2, 78);
insert into Score (StudentId, CourseId, Score) values (1, 3, 65);
insert into Score (StudentId, CourseId, Score) values (1, 4, 90);

insert into Score (StudentId, CourseId, Score) values (2, 1, 60);
insert into Score (StudentId, CourseId, Score) values (2, 2, 77);
insert into Score (StudentId, CourseId, Score) values (2, 3, 68);
insert into Score (StudentId, CourseId, Score) values (2, 4, 88);

insert into Score (StudentId, CourseId, Score) values (3, 1, 88);
insert into Score (StudentId, CourseId, Score) values (3, 2, 45);
insert into Score (StudentId, CourseId, Score) values (3, 3, 66);
insert into Score (StudentId, CourseId, Score) values (3, 4, 75);
go

--	江文、数据库高级应用、56
--	江文、javascript编程基础、80
--	江文、web前端程序设计基础、75
--	江文、动态网页设计.net基础、66
--	钟琪、数据库高级应用、88
--	钟琪、javascript编程基础、79
--	钟琪、web前端程序设计基础、72
--	钟琪、动态网页设计.net基础、85

insert into Score (StudentId, CourseId, Score) values (4, 1, 56);
insert into Score (StudentId, CourseId, Score) values (4, 2, 80);
insert into Score (StudentId, CourseId, Score) values (4, 3, 75);
insert into Score (StudentId, CourseId, Score) values (4, 4, 66);

insert into Score (StudentId, CourseId, Score) values (5, 1, 88);
insert into Score (StudentId, CourseId, Score) values (5, 2, 79);
insert into Score (StudentId, CourseId, Score) values (5, 3, 72);
insert into Score (StudentId, CourseId, Score) values (5, 4, 85);


--	曾小林、数据库高级应用、68
--	曾小林、javascript编程基础、88
--	曾小林、web前端程序设计基础、73
--	曾小林、动态网页设计php基础、63
--	欧阳天天、数据库高级应用、84
--	欧阳天天、javascript编程基础、90
--	欧阳天天、web前端程序设计基础、92
--	欧阳天天、动态网页设计php基础、78
--	徐长卿、数据库高级应用、58
--	徐长卿、javascript编程基础、59
--	徐长卿、web前端程序设计基础、65
--	徐长卿、动态网页设计php基础、75
--	李逍遥、数据库高级应用、48
--	李逍遥、javascript编程基础、67
--	李逍遥、web前端程序设计基础、71
--	李逍遥、动态网页设计.net基础、56
insert into Score (StudentId, CourseId, Score) values (6, 1, 68);
insert into Score (StudentId, CourseId, Score) values (6, 2, 88);
insert into Score (StudentId, CourseId, Score) values (6, 3, 73);
insert into Score (StudentId, CourseId, Score) values (6, 5, 63);

insert into Score (StudentId, CourseId, Score) values (7, 1, 84);
insert into Score (StudentId, CourseId, Score) values (7, 2, 90);
insert into Score (StudentId, CourseId, Score) values (7, 3, 92);
insert into Score (StudentId, CourseId, Score) values (7, 5, 78);

insert into Score (StudentId, CourseId, Score) values (8, 1, 58);
insert into Score (StudentId, CourseId, Score) values (8, 2, 59);
insert into Score (StudentId, CourseId, Score) values (8, 3, 65);
insert into Score (StudentId, CourseId, Score) values (8, 5, 75);

insert into Score (StudentId, CourseId, Score) values (9, 1, 48);
insert into Score (StudentId, CourseId, Score) values (9, 2, 67);
insert into Score (StudentId, CourseId, Score) values (9, 3, 71);
insert into Score (StudentId, CourseId, Score) values (9, 5, 56);
insert into Score (StudentId, CourseId, Score) values (9, 5, 56);
go

--------------------
-- 删除数据部分
--------------------
-- 分数表这边 分数表 最后一条插入重复了，请帮忙删除这条数据
select * from Score order by ScoreId desc;
delete from Score where ScoreId = 37;

--------------------
-- 修改数据部分
--------------------
-- 计算机应用技术班 的 欧阳天天 生日写错了，正确的生日应该是：2000-04-06，请用sql进行修改。（update）
update Student set StudentBirth='2000-04-06' where StudentId = 7;
go
-- 计算机应用技术班 的 徐长卿 的 javascript编程基础 分数填错，正确的分数应该是：61，请用sql进行修改。（update）
update Score set Score = 61 where StudentId = 8 and CourseId = 2;
go


select * from Class;
select * from Student;
select * from Course;
select * from ClassCourse;
select * from Score;
select * from Student where ClassId = 1;
select * from Student where StudentSex = 2;
select * from Student where StudentBirth >= '2000-01-01' and StudentBirth < '2001-01-01';

create table Stuinfo(
 stuNO nvarchar(50) not null ,
 stuName nvarchar(50) not null,
 stuAge int not null,
 stuAddress nvarchar(50),
 stuSeat int  not null,
 stuSex int not null
)
insert into Stuinfo (stuNO,stuName,stuAge,stuAddress,stuSeat,stuSex )
values('s2501','张秋利','20','美国硅谷','1','1'),
('s2502','李斯文','18','湖北武汉','2','0'),
('s2503','马文才','22','湖南长沙','3','1'),
('s2504','欧阳俊雄','21','湖北武汉','4','0'),
('s2505','梅超风','20','湖北武汉','5','1'),
('s2506','陈旋风','19','美国硅谷','6','1'),
('s2507','陈风','20','美国硅谷','7','0');

select * from Stuinfo;

create table Stuexam(
examNO int not null,
stuNO nvarchar(50) not null,
writtenExam int not null,
labExam int not null,
)
insert into Stuexam (examNO,stuNO,writtenExam ,labExam)
values('1','s2501','50','70'),
('2','s2502','60','65'),
('3','s2503','86','85'),
('4','s2504','40','80'),
('5','s2505','70','90'),
('6','s2506','85','90')
select * from Stuexam ;





--1.查询学生信息表（stuinfo）中所有列信息，给每列取上中文名称
select stuNO as 学生ID, stuName as 学生姓名 ,stuAge as 学生年龄,stuAddress as 学生地址,stuSeat as 学生序号,stuSex as 学生性别 from Stuinfo;

--2.查询学生信息表（stuinfo）中的姓名，年龄和地址三列的信息
select stuName,stuAge,stuAddress from Stuinfo ;
--3.查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字
  --注意：要用三种方法
  select examNO as 学号, stuNO as 学生ID ,writtenExam as 笔试,labExam as 机试 from Stuexam ;
--4.查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+:+手机号 组成新列 “邮箱”
select StuName+'@'+stuAddress from stuInfo;
--5.查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分这四列的信息
  select examNO as 学号, stuNO as 学生ID ,writtenExam as 笔试,labExam as 机试 from Stuexam ;

--12. 查询姓是欧阳的学生。
select * from Student where StudentName like '%欧阳%';
--13. 询地址是桂林市的学生。
select * from Student where StudentAddress like '%桂林市%';
--14. 查询姓李的学生，并且是三个字的。
select * from Student where StudentName like '李__';
--15. 查询 软件一班 有几个学生。
select count(0) from Student where ClassId =1; 
--16. 查询 软件一班 有几门课程。
select count(0) from ClassCourse  where ClassId =1; 
--17. 查询 刘正(学生编号为1) 的总分。
select sum(Score ) from Score where StudentId =1;
--18. 查询所有学生 数据库高级应用 的平均分。
select * from Score where CourseId =1;
select sum(Score ) 总分,count(0) 学生总数 from Score where CourseId =1;
select avg(Score ) from Score where CourseId =1;
--19. 查询 所有学生 的总分。
select * from Score ;
select StudentId, sum(Score) 总分 from Score 
group by StudentId ;
--20. 查询 所有学生 的平均分。
select * from Score ;
select StudentId, avg(Score ) 平均分 from Score 
group by StudentId ;
--21. 查询 所有学生 的平均分，并且按照平均分从高到低排列。
select StudentId, avg(Score ) 平均分 from Score 
group by  StudentId order by  avg(Score) desc;
--22. 查询 所有学生中 平均分 大于80分的学生。（聚合查询 + 分组 + having）
select * from Score ;
select StudentId, avg(Score ) 平均分 from Score 
group by StudentId 
having  avg(Score )>80;

--8.查询学生信息表（stuInfo）中前3行记录 
select top 3  * from Stuinfo ;
--9.查询学生信息表（stuInfo）中前4个学生的姓名和座位号
select top 4 stuName,stuSeat  from Stuinfo; 
--10.查询学生信息表（stuInfo）中一半学生的信息
select top 50 percent * from Stuinfo ;
--11.将地址是湖北武汉，年龄是20的学生的所有信息查询出来
select * from Stuinfo  where stuAddress  like '%湖北武汉%' and stuAge =20;
--12.将机试成绩在60-80之间的信息查询出来，并按照机试成绩降序排列（用两种方法实现）
select * from Stuexam where labExam  between 60 and 80 order by labExam  desc;
select * from Stuexam where labExam >=60 and labExam <= 80 order by labExam  desc;
--13.查询来自湖北武汉或者湖南长沙的学生的所有信息（用两种方法实现）
select * from Stuinfo where stuAddress like '%湖北武汉%' or stuAddress like '%湖南长沙%'
--14.查询出笔试成绩不在70-90之间的信息,并按照笔试成绩升序排列（用两种方法实现）
select * from Stuexam where writtenExam not between 70 and 90 order by writtenExam asc;
select * from Stuexam where writtenExam <70 or writtenExam  >90 order by writtenExam asc;
--15.查询年龄没有写的学生所有信息
select * from Student  where StudentBirth  is null;
--16.查询年龄写了的学生所有信息
select * from Student where StudentBirth is not null;
--17.查询姓张的学生信息
select * from Stuinfo  where stuName  like '%张%'
--18.查询学生地址中有‘湖’字的信息
select * from Stuinfo where stuAddress like '%湖%'
--19.查询姓张但名为一个字的学生信息
select * from Stuinfo  where stuName  like '张_'
--20.查询姓名中第三个字为‘俊’的学生的信息，‘俊’后面有多少个字不限制
select * from Stuinfo  where stuName  like '__俊'
--21.按学生的年龄降序显示所有学生信息
select * from Stuinfo order by stuAge desc; 
--22.按学生的年龄降序和座位号升序来显示所有学生的信息
select * from Stuinfo order by stuAge desc, stuSeat asc;
--23显示笔试第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 * from Stuexam order by writtenExam  desc; 
--24.显示机试倒数第一名的学生的考试号，学号，笔试成绩和机试成绩
select top 1 * from Stuexam order by labExam asc;


----------------------
---
--4.2
create table orders(
orderID int primary key not null identity(1,1),
orderDate nvarchar(50) not null,
)
select * from orders ;
insert into orders (orderDate )
values ('2008-01-12 00:00:00.000'),
('2008-02-10 00:00:00.000'),
('2008-02-15 00:00:00.000'),
('2008-03-10 00:00:00.000');


create table orderltem(
itemID int primary key identity(1,1) not null,
orderid int  references orders(orderid),
itemType nvarchar(50),
itemName nvarchar(50),
theNumber int,
theMoney int,
)
select * from orderltem ;
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(1,'文具','笔',72,2);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(1,'文具','尺',10,1);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(1,'体育用品','篮球',1,56);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(2,'文具','笔',36,2);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(2,'文具','固体胶',20,3);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(2,'日常用品','透明胶',2,1);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(2,'体育用品','羽毛球',20,3);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(3,'文具','订书机',20,3);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(3,'文具','订书针',10,3);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(3,'文具','裁纸刀',5,5);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(4,'文具','笔',20,2);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(4,'文具','信纸',50,1);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(4,'日常用品','毛巾',4,5);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(4,'日常用品','透明胶',30,1);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(4,'体育用品','羽毛球',20,3);
--1.查询所有订单订购的所有物品数量总和
select sum(theNumber) 数量总和 from orderltem ;
--2.查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价

select orderid ,sum(theNumber) 所有物品数量,avg(theMoney ) 平均单价 from orderltem 
where orderid<3
group by orderid 
having avg(theMoney )<10

--3.查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
select sum(theNumber) 所有物品数量,avg(theMoney ) 平均单价 from orderltem 
where theNumber>50
group by theNumber 
having avg(theMoney )<10
--4.查询每种类别的产品分别订购了几次，例如：
--文具      9
--体育用品  3
--日常用品  3
select itemType ,count(itemType  ) 订购次数 from orderltem 
group by itemType 
--5.查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select orderid ,sum(theNumber ) 订购总数量, avg(theMoney ) 平均单价 from orderltem 
group by orderid 
having sum(theNumber)>100
--6.查询每种产品的订购次数，订购总数量和订购的平均单价，例如：

-- 产品名称   订购次数  总数量   平均单价 
--笔           3       120       2
select itemName 产品名称 , count(itemType ) 订购次数, sum(theNumber ) 总数量, avg(theMoney ) 平均单价 from orderltem 
group by itemName  

---------------------
-------
--作业三
