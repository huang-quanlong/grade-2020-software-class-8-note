use master
go

--判断数据库是否存在
if exists(select * from sys.databases where name='bbs')
begin 
	drop database bbs  --存在删库
end
go


--创建数据库
create database bbs
go

--使用数据库
use bbs
go
	

--创建用户信息表（bbsUsers）
create table bbsUsers
(
	UID int identity(1,1) primary key,	--用户编号  UID int 主键  标识列
	uName varchar(10) unique not null,	--用户名    uName varchar(10)  唯一约束 不能为空
	uSex  varchar(2) check(uSex='男' or uSex='女') not null,	--性别      uSex  varchar(2)  不能为空 只能是男或女
	uAge  int not null check(uAge>=15 and uAge<=60),	--年龄      uAge  int  不能为空 范围15-60
	uPoint  int not null  check(uPoint>=0)	--积分      uPoint  int 不能为空  范围 >= 0
)
go


--创建版块表（bbsSection）
create table bbsSection
(
	sID  int identity(1,1) primary key,	--版块编号  sID  int 标识列 主键
	sName  varchar(10) not null,	--版块名称  sName  varchar(10)  不能为空
	sUid   int references bbsUsers(UID)	--版主编号  sUid   int 外键  引用用户信息表的用户编号
)
go


--创建主贴表（bbsTopic）	
create table bbsTopic
(
	tID  int identity(1,1) primary key,	--主贴编号  tID  int 主键  标识列
	tUID  int references bbsUsers(UID),	--发帖人编号  tUID  int 外键  引用用户信息表的用户编号
	tSID  int references bbsSection(sID),	--版块编号    tSID  int 外键  引用版块表的版块编号    （标明该贴子属于哪个版块）
	tTitle  varchar(100) not null,	--贴子的标题  tTitle  varchar(100) 不能为空
	tMsg  nvarchar(max) not null,	--帖子的内容  tMsg  nvarchar(max)  不能为空
	tTime  datetime,	--发帖时间    tTime  datetime  
	tCount  int	--回复数量    tCount  int
)
go
 

--创建回帖表（truncate table）
create table bbsReply
(
	rID  int identity(1,1) primary key,--回贴编号  rID  int 主键  标识列，
	rUID  int references bbsUsers(UID),--回帖人编号  rUID  int 外键  引用用户信息表的用户编号
	rTID  int references bbsTopic(tID),--对应主贴编号    rTID  int 外键  引用主贴表的主贴编号    （标明该贴子属于哪个主贴）
	rMsg  nvarchar(max) not null,--回帖的内容  rMsg  nvarchar(max)  不能为空
	rTime  datetime,--回帖时间    rTime  datetime 
)
go

--为用户信息表插入数据
insert into bbsUsers(uName,uSex,uAge,uPoint)
select '小雨点','女',20,0 union
select  '逍遥','男',18,4 union 
select '七年级生','男',19,2 union 
select '小丸子','女',17,12 union
select '小心大熊','女',28,15
go


--为版块表插入数据
insert into bbsSection(sName,sUid) values ('技术交流',1)
insert into bbsSection(sName,sUid) values('读书世界',2)
insert into bbsSection(sName,sUid) values('生活百科',1)
insert into bbsSection(sName,sUid) values('八卦区',2)
go
	
--为主贴表插入数据	
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount)
values (2,4,'范跑跑','谁是范跑跑','2008-7-8',1)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount)
values (3,1,'.NET','与JAVA的区别是什么呀？','2008-9-1',2)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount)
values (1,3,'今年夏天最流行什么 ','有谁知道今年夏天最流行什么呀？','2008-9-10',0)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount)
values (4,2,'读书使我快乐! ','你们都喜欢读什么书呢?','2011-9-10',13)

insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount)
values (1,3,'先有鸡还是先有蛋，终于有答案了!','一个英国的科学家在经过了一系列的研究之后，发现鸡蛋的形成是需要一种特别的蛋白质，这个特殊的蛋白质是在母鸡们的身体中产生的，如果没有这种特殊的蛋白质就不会形成鸡蛋。换句话说，如果没有母鸡就不会产生这些个物质，也就不会有鸡蛋。因此，先有鸡后有蛋才是符合科学。接下来，我们就不禁又要问：既然是先有鸡后有蛋，那么鸡又是怎么来的呢？',getdate(),0)
go

--为回贴表插入数据
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(1,1,'2008-7-10','就那啥，网红呗')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(1,2,'2008-9-2','.net是一个跨语言的平台；java是一个开源的跨平台的语言；')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(2,2,'2008-9-12','如果开发windows应用那还是.net 好一些，因为windows和.net都是微软的产品，所以在开发window应用方面.net更兼容一些。')

insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 20:43:00','秘密花园')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 21:43:00','草房子')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 20:43:00','我爸爸')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 21:43:00','活了一百万次的猫')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 20:43:00','红字')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 21:43:00','洛丽塔')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 20:43:00','哈姆雷特')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 21:43:00','苏菲的世界')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 20:43:00','小王子')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 21:43:00','红鞋子')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 20:43:00','一片叶子落下来')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 21:43:00','沉重的肉身')
insert into bbsReply(rUID,rTID,rtime,rMsg)
values(5,4,'2011-9-11 20:43:00','文化苦旅')
go

--查询用户信息表的所有数据
select * from bbsUsers

--查询版块表的所有数据
select * from bbsSection

--查询主贴表的所有数据
select * from bbsTopic

--查询回帖表的所有数据
select * from bbsReply

--1.在主贴表中统计每个版块的发帖总数
select tSID,count(tSID) 发帖总数 from bbsTopic 
group by tSID 
--2.在回帖表中统计每个主贴的回帖总数量
select rTID  ,count(rTID  ) 回帖总数量 from bbsReply
group by rTID  
--3.在主贴表中统计每个用户的发的主帖的总数
select tUID, count(tUID ) 每个用户发帖总数 from bbsTopic
group by tUID 
--4.在主贴表中统计每个用户发的主贴的回复数量总和
select tUID , sum(tCount ) 回帖数量总和 from bbsTopic
group by tUID 
--5.在主贴表中查询每个版块的主贴的平均回复数量大于3的版块的平均回复数量
select tSID ,avg(tCount ) 平均回复大于三数量 from bbsTopic
group by tSID 
having avg(tCount )>3
--6.在用户信息表中查询出积分最高的用户的用户名，性别，年龄和积分
select top 1 * from bbsUsers
order by uPoint  desc
--7.在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来
select * from bbsTopic where tMsg  like '%快乐%' or tTitle  like '%快乐%'
--8.在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）
select * from bbsUsers where uAge >15 and uAge <20 and uPoint >10;
select * from bbsUsers where uAge between 15 and 20 and uPoint >10;
--9.在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来
select * from bbsUsers where uName like '小_大%';
--10.在主贴表（bbsTopic）中将在2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，并且为列取上对应的中文列名
select tTitle 标题 ,tMsg 内容 from bbsTopic where tTime >'2008-9-10 12:00:00' and tCount >10;
--11.在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖人编号和回复数量查询出来
select tUID ,count(tCount) 回复数量 from bbsTopic where tTitle like '%!%'
group by tUID 
