use master
go

--判断数据库是否存在
if exists(select * from sys.databases where name='bbs')
begin 
	drop database bbs  --存在删库
end
go


--创建数据库
create database bbs
go

--使用数据库
use bbs
go
	

--创建用户信息表（bbsUsers）
create table bbsUsers
(
	UID int identity(1,1) primary key,	--用户编号  UID int 主键  标识列
	uName varchar(10) unique not null,	--用户名    uName varchar(10)  唯一约束 不能为空
	uSex  varchar(2) check(uSex='男' or uSex='女') not null,	--性别      uSex  varchar(2)  不能为空 只能是男或女
	uAge  int not null check(uAge>=15 and uAge<=60),	--年龄      uAge  int  不能为空 范围15-60
	uPoint  int not null  check(uPoint>=0)	--积分      uPoint  int 不能为空  范围 >= 0
)
go
insert into bbsUsers (uName ,uSex ,uAge ,uPoint ) values('小雨点','女','20','0')
insert into bbsUsers (uName ,uSex ,uAge ,uPoint ) values('逍遥','男','18','4')
insert into bbsUsers (uName ,uSex ,uAge ,uPoint ) values('七年级生','男','19','2')


--创建版块表（bbsSection）
create table bbsSection
(
	sID  int identity(1,1) primary key,	--版块编号  sID  int 标识列 主键
	sName  varchar(10) not null,	--版块名称  sName  varchar(10)  不能为空
	sUid   int references bbsUsers(UID)	--版主编号  sUid   int 外键  引用用户信息表的用户编号
)
go
insert into bbsSection (sName,sUid  ) values('技术交流',1),
('读书世界',3),
('生活百科',1),
('八卦区',3)


--创建主贴表（bbsTopic）	
create table bbsTopic
(
	tID  int identity(1,1) primary key,	--主贴编号  tID  int 主键  标识列
	tUID  int references bbsUsers(UID),	--发帖人编号  tUID  int 外键  引用用户信息表的用户编号
	tSID  int references bbsSection(sID),	--版块编号    tSID  int 外键  引用版块表的版块编号    （标明该贴子属于哪个版块）
	tTitle  varchar(100) not null,	--贴子的标题  tTitle  varchar(100) 不能为空
	tMsg  nvarchar(max) not null,	--帖子的内容  tMsg  nvarchar(max)  不能为空
	tTime  datetime,	--发帖时间    tTime  datetime  
	tCount  int	--回复数量    tCount  int
)
go
 insert into bbsTopic (tUID ,tSID ,tTitle ,tMsg ,tTime ,tCount ) values(2,5,'范跑跑','谁是范跑跑','2008-7-8',1),
(3,2,'.NET','与Java的区别是什么呀？','2008-9-1',2),
(1,4,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀？','2008-9-10',0)
--创建回帖表（truncate table）
create table bbsReply
(
	rID  int identity(1,1) primary key,--回贴编号  rID  int 主键  标识列，
	rUID  int references bbsUsers(UID),--回帖人编号  rUID  int 外键  引用用户信息表的用户编号
	rTID  int references bbsTopic(tID),--对应主贴编号    rTID  int 外键  引用主贴表的主贴编号    （标明该贴子属于哪个主贴）
	rMsg  nvarchar(max) not null,--回帖的内容  rMsg  nvarchar(max)  不能为空
	rTime  datetime,--回帖时间    rTime  datetime 
)
go
insert into bbsReply (rUID ,rTID ,rMsg ,rTime ) values(2,1,'不知道','2009-10-2'), 
(3,2,'不知道','2009-10-2'),
(3,2,'不知道','2009-10-2')
select * from bbsUsers ;
select * from bbsSection ;
select * from bbsTopic  ;
select * from bbsReply ;


	--5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）
alter table bbsReply drop constraint FK__bbsReply__rTID__1DE57479
delete from bbsTopic where tUID=2
alter table bbsReply drop constraint FK__bbsReply__rUID__1CF15040
delete from bbsUsers where uName ='逍遥';
	--6.因为小雨点发帖较多，将其积分增加10分
	update bbsUsers set uPoint =uPoint +10 where UID =1;
	--7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
alter table bbsTopic drop constraint FK__bbsTopic__tSID__1A14E395
delete from bbsSection where sID =4
	--8.因回帖积累太多，现需要将所有的回帖删除
	delete from bbsReply ;