create database Student

create table Class (
ClassId int primary key identity(1,1) not null ,
  ClassName nvarchar(50) not null ,

)
  select * from Class;
  create table Student (
StudentId int primary key identity(1,1) not null ,
StudentName nvarchar(50) not null ,
StudentSex tinyint not null default'3',
StudentBirth date ,
StudentAddress nvarchar(255) not null default'',
Classld int not null ,
  )
    select * from Student;
create table Course (
Courseld int primary key identity(1,1) not null ,
CourseName 	nvarchar(50) not null,
CourseCredit tinyint not null default'0',
)
 select * from Course;
 create table ClassCourse(
 ClassCourseId int primary key identity(1,1) not null ,
 ClassId int not null,
 CourseId int not null,
 )
  select * from ClassCourse;
   create table Score(
ScoreId	int primary key identity(1,1) not null ,
StudentId int not null,
CourseId int not null,
Score int not null, 
   )
select * from Score;
alter table Student add StudentIdentityCard nvarchar(20) not null  default '';

alter table Score add IsResit tinyint not null  default '';

alter table Class add constraint UNQ_Class_ClassName unique(ClassName);

alter table Course add constraint UNQ_Course_CourseName unique(CourseName);

alter table Student add constraint DF_Student_Classld default(0) for Classld;

alter table Student add constraint CK_Student_StudentSex check(StudentSex=1 or StudentSex=2 or StudentSex=3);

alter table Score add constraint CK_Score_Score check(Score>=0);

alter table Course add constraint CK_Course_CourseCredit check(CourseCredit>0);

alter table Student add constraint FK_Student_Classld foreign key (Classld) references Class(ClassId);

alter table ClassCourse add constraint FK_ClassCourse_CourseId foreign key (CourseId) references Course(Courseld);

alter table Score add constraint FK_Score_StudentId foreign key (StudentId) references Student(StudentId);

alter table Score add constraint FK_Score_CourseId foreign key (CourseId) references Course(Courseld);