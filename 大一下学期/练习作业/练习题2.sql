create database HOUSE_DB;
go 
use HOUSE_DB;


create table HOUSE_TYPE(
typeid	int	not null primary key identity(1,1),	--类型编号
typename	varchar(50)	not null 		--类型名称

);

create table HOUSE(
house_id	int	not null primary key identity(1,1),    --	房屋编号
house_name	varchar(50)	not null,		--房屋名称
house_price	float	not null default(0),	--	房租
typeid	int	not null	references HOUSE_TYPE(typeid)	--房屋类型
);


insert into HOUSE_TYPE (typename )
values('小户型'),
('经济型'),
('别墅')
select * from HOUSE_TYPE 

insert into HOUSE (house_name ,house_price )
values ('月亮湾','750'),
('鼎盛骏景','2000'),
('黄金海岸','5000')