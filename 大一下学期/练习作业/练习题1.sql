---创建商品数据库（GoodsDB），然后建立两张表，GoodsType（商品类型表），GoodsInfo（商品信息表）
create database GoodsDB;
go

use GoodsDB;

create table GoodsType(   -----商品类型表
TypeID	int	primary key identity(1,1) not null,---商品类型编号
TypeName	nvarchar (20)	not null	----商品类型名称
);

create table GoodsInfo(  -----商品信息表
GoodsID		int	not null primary key identity(1,1),----商品编号
GoodsName		nvarchar(20) not null,  -----商品名称
GoodsColor		nvarchar(20) not null,		--商品颜色
GoodsBrand		nvarchar (20),	--商品品牌
GoodsMoney		money	not null,	  --商品价格
TypeID		int		references 	GoodsType(TypeID ) --商品类型编号
);

insert into GoodsType(TypeName)
values('服装内衣')
,('鞋包配饰')
,('手机数码'); 

insert into GoodsInfo(GoodsName,GoodsColor,GoodsBrand,GoodsMoney,TypeID)
values('提花小西装','红色','菲曼琪','300','1')
,('百搭短裤','绿色','哥弟','100','1')
,('无袖背心','红色','阿依莲','700','1')
,('低帮休闲鞋','红色','菲曼琪','900','2')
,('中跟单鞋','绿色','哥弟','400','2')
,('平底鞋','白色','阿依莲','200','2')
,('迷你照相机','红色','尼康','500','3')
,('硬盘','黑色','希捷','600','3')
,('显卡','黑色','技嘉','800','3');

select  * from GoodsType
select * from GoodsInfo

--3、查询价格最贵的商品名称，商品颜色和商品价格，要求使用别名显示列名
select top 1 GoodsName 商品名称 ,GoodsColor 商品颜色  ,GoodsMoney 商品价格   from GoodsInfo order by GoodsMoney desc;
--4、按商品类型编号分组查询商品最高价格，最低价格和平均价格，要求使用别名显示列名
select TypeID 商品类型编号,max(GoodsMoney) 最高价格,min(GoodsMoney)最低价格,avg(GoodsMoney) 平均价格 from GoodsInfo group by TypeID
--5、查询商品信息所有列，要求商品颜色为红色，价格在300~600之间
select * from GoodsInfo where GoodsColor like '红色' and GoodsMoney between 300 and 600









